---
title: About Me
subtitle: This is a short page about me and my work
img_path: /images/sine-wav-bak-ver.jpg
seo:
  title: About Me
  description: A page about me and my work
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: About Me
      keyName: property
    - name: 'og:description'
      value: A page about me and my work
      keyName: property
    - name: 'og:image'
      value: images/about.jpg
      keyName: property
      relativeUrl: true
    - name: 'twitter:card'
      value: summary_large_image
    - name: 'twitter:title'
      value: About Me
    - name: 'twitter:description'
      value: A page about me and my work
    - name: 'twitter:image'
      value: images/about.jpg
      relativeUrl: true
layout: page
---
A passionate Web Developer, Electrical Engineer, Musician & Producer

*   🔭 Contract Web Development **Relational Concepts**

*   🌱 I'm currently learning **React/Redux, Python, Java, Express, jQuery**

*   👯 I'm looking to collaborate on [Any web audio or open source educational tools.](https://goofy-euclid-1cd736.netlify.app/core-site/index.html)

*   🤝 I'm looking for help with [Learning React](https://github.com/bgoonz/React-Practice)

*   👨‍💻 All of my projects are available at <https://bgoonz.github.io/>

*   📝 I regularly write articles on [medium](https://bryanguner.medium.com/) && [Web-Dev-Resource-Hub](https://web-dev-resource-hub.netlify.app/)

*   💬 Ask me about **Anything:**

*   📫 How to reach me [**bryan.guner@gmail.com**](mailto:bryan.guner@gmail.com)

*   ⚡ Fun fact **I played Bamboozle Music Festival at the Meadowlands Stadium Complex when I was 14.**

***

### i really like music 🎧

#### What words of wisdom do you live by?&#xA;&#xA;

> "Be the change that you wish to see in the world."

**― Mahatma Gandhi**

## As a hobby,  I write, play and record music.

##### Coolest Project:

Successfully completed and delivered a platform to digitize a guitar signal and perform filtering before executing frequency & time domain analysis to track a current performance against prerecorded performance.

*   Implemented the Dynamic Time Warping algorithm in C++ and Python to autonomously activate or adjust guitar effect at multiple pre-designated section of performance.

# Who I am in Pictures:

![](/\_static/app-assets/images/collage-lite.gif)
